;;; rules-line.lisp - new line rules mechanism {{{
(defpackage :bopwiki-linerules
	;; import print l only when debugging so grep shows where debugging is
	;; (:import-from :grevillea #:rintl)
	(:use :common-lisp
		:bopwiki-core-structs
			;; make-meta-line
		:bopwiki-util :bopwiki-core-table :bopwiki-linerules-parse)
	(:local-nicknames
		(:table :bopwiki-core-table)
		(:parse :bopwiki-linerules-parse)
	)
	(:export
		;; line rule struct
		#:line-rule #:make-line-rule #:line-rule-p
		#:line-rule-pkgname #:line-rule-sphere #:line-rule-name #:line-rule-label #:line-rule-parser
		#:line-rule-icon
			;; note: 'LINE-RULE-ICON is reused as an arbitrary symbol to identify icons
		#:line-rule-displayp #:line-rule-outlinkp #:line-rule-backlinkp #:line-rule-threadp #:line-rule-aliasp #:line-rule-deferp #:line-rule-commandp
		
		;; initial line rule list
		#:*line-rules* #:register-line-rule #:register-line-rules
		;; line rule table
		#:*line-rule-table* #:create-rule-table
		#:find-line-rule
		#:greatest-icon-length
		#:put-rule
		#:find-meta-rules
		
		;; meta line tools
		#:duplicate-meta-line
		#:meta-line-attribute #:meta-lines-with
		#:non-revision-metas
		#:metas-with-rel
		
		;; entry filters
		#:*parse-filter-list* #:register-parse-filter
		#:*table-filter-list* #:register-table-filter
		#:*tablepost-filter-list* #:register-tablepost-filter
		#:run-table-filters
		
		#:line-meta-info
		
		#:rule-parser-value #:rule-parser-label
		;; LATER: check if outdated or re-exporting
		
		;; rules
		#:rule-cxt #:rule-rem
		
		;; filters
		#:basic-links-filter #:basic-backlinks-filter #:alias-filter
))
(in-package :bopwiki-linerules)

(defstruct line-rule  ; rule to be returned by meta line data function {{{
	sphere  ; kind of line rule: meta, formatting, etc
	pkgname  ; package name
	
	name   ; rule name string, originally used for effect of rule - to change?
	icon   ; abbreviated icon to display in page footer
	label  ; caption to use for aria-label etc
	parser  ; for extracting information out of line
	
	displayp  ; whether to render line in entry footer
	outlinkp  ; whether this link goes to something outward/"forward"
	backlinkp ; whether this link attaches to something backward
	threadp   ; whether to recurse into link's thread
	
	aliasp   ; whether to make this line's value an alias for its entry
		;; LATER: should this be turned into a :rel used with :commandp ??
	deferp   ; whether to hide entry from main set of "real entries" when this line is present
	commandp ; whether this rule calls meta line's :rel function on :value - tentative
) ; }}}

;;; }}}


;;; basic meta line tools {{{

(defun duplicate-meta-line (line  &key kind kindsym rel value label num raw) ; {{{
	(make-meta-line
		:kind     (if (null kind)    (meta-line-kind line)    kind)
		:kindsym  (if (null kindsym) (meta-line-kindsym line) kindsym)
		:rel      (if (null rel)     (meta-line-rel line)     rel)
		:value    (if (null value)   (meta-line-value line)   value)
		:label    (if (null label)   (meta-line-label line)   label)
		:num      (if (null num)     (meta-line-num line)     num)
		:raw      (if (null raw)     (meta-line-raw line)     raw)
	;; LATER: there really ought to be a more efficient way to do this by iterating the slots in a structure?
)) ; }}}

(defun meta-line-attribute (meta-line has-attribute-p  &key data) ; examine line-rule {{{
	(let (kindsym rule valid-line-p valid-accessor-p)
	
	;; throw a warning if HAS-ATTRIBUTE-P is not a function
	(unless (setq valid-accessor-p (functionp #'has-attribute-p))
		(warn "Slot to access was not given as an accessor function"))
	
	(cond
		;; if passed :data T, operate on meta line data
		((not-null data)
			(setq valid-line-p (meta-line-p meta-line))
			(setq rule meta-line))
		(t
			;; get meta-line's line-rule symbol, assuming it is a function returning a 'line-rule
			(setq kindsym (meta-line-kindsym meta-line))
			;; look up meta line's line-rule
			(setq rule (funcall kindsym))
			;; if kindsym does not reference a function, throw a warning
			(unless (setq valid-line-p (line-rule-p rule))
				(warn "Could not get LINE-RULE data of META-LINE"))
	))
	
	;; if attribute of line-rule can reasonably be accessed, return it
	(if (and valid-accessor-p valid-line-p)
		(funcall has-attribute-p rule)
		nil)
)) ; }}}

(defun meta-lines-with (meta-lines has-attribute-p  &key without data) ; filter meta lines {{{
;; doc {{{
"Return meta lines with/without specified attribute.

META-LINES - a list of 'meta-line structs to examine the 'line-rule of
HAS-ATTRIBUTE-P - a predicate function to call on a particular line-rule slot, returning T or NIL. for example, you can use 'line-rule-deferp in order to check :deferp for T and return that the entry is some kind of revision that will be hidden"
;; }}}

	(mapcan  ; redo list with the following procedure:
		(lambda (link)
			(let (has-attribute-result)
			
			(setq
				has-attribute-result (meta-line-attribute link has-attribute-p :data data)
				has-attribute-result
					(if (null without)  ; "normally", when not passed :without T,
						;; return whether truth test is true
						(not (null has-attribute-result))
						;; when passed :without T, return whether truth test is NIL
						(null has-attribute-result))
			)
			
			;; LATER: allow multiple truth tests in a list
			
			;; if the meta line has (or is :without) the attribute...
			(if (null has-attribute-result)
				nil
				(list link))  ; ...keep it in the list
		))
		meta-lines)
) ; }}}

(defun non-revision-metas (meta-lines) ; return meta lines without :deferp
	(meta-lines-with meta-lines 'line-rule-deferp  :without t))

(defun metas-with-rel (meta-lines rel) ; return meta-lines with given :rel in their data
	(meta-lines-with
		meta-lines
		(lambda (line)
			(equal (meta-line-rel line) rel))
		:data t))
;;

;;; }}}


;;; initial line rule & filter lists {{{

(defparameter *line-rules* nil)  ; where meta line rules will be registered

(defun register-line-rule (rule) ; add line rule to rule list {{{
"add line rule (meta, formatting, etc) from particular package"
	(let ((qualified (find-symbol
		(string rule) (package-name (symbol-package rule))
		)))
	(setq *line-rules* (cons qualified *line-rules*))
)) ; }}}

(defun register-line-rules (rules) "add multiple line rules at once" ; {{{
	(dolist (rule rules)
		(register-line-rule rule))
) ; }}}


(defparameter *parse-filter-list* nil)  ; filters run when entry body is loaded

(defun register-parse-filter (rule) ; add filter to list {{{
"tentative until i figure out whether this qualify symbol thing is necessary"
;; LATER: do it
	(let ((qualified (find-symbol
		(string rule) (package-name (symbol-package rule))
		)))
	(setq *parse-filter-list* (cons qualified *parse-filter-list*))
)) ; }}}

(defparameter *table-filter-list* nil)  ; filters run on metadata table row

(defun register-table-filter (rule) ; add filter to list {{{
"tentative until i figure out whether this qualify symbol thing is necessary"
;; LATER: do it
	(let ((qualified (find-symbol
		(string rule) (package-name (symbol-package rule))
		)))
	(setq *parse-filter-list* (cons qualified *parse-filter-list*))
)) ; }}}

(defparameter *tablepost-filter-list* nil)  ; run on simple row immediately after table

(defun register-tablepost-filter (rule) ; add filter to list {{{
"tentative until i figure out whether this qualify symbol thing is necessary"
;; LATER: do it
	(let ((qualified (find-symbol
		(string rule) (package-name (symbol-package rule))
		)))
	(setq *tablepost-filter-list* (cons qualified *tablepost-filter-list*))
)) ; }}}


(defun run-table-filters (full-body)
	(dolist (filter *table-filter-list*)
		(setq full-body (funcall filter full-body)))
	full-body
)


;;; }}}

;;; fancier line rules registry {{{

(defparameter *line-rule-table* nil)

;; LATER: check if unused/outdated?
(defun line-rule-key (rule) ; key to file rule in rule table {{{
	(let ((pkgname (line-rule-pkgname rule)) (name (line-rule-name rule))
		;(sphere (line-rule-sphere rule))
		)
	(format nil "~a:~a" pkgname name)
)) ; }}}

(defun greatest-icon-length (&key set-value) ;{{{
	(table:table-statistic *line-rule-table* :hash-key "BOP:MAX-LEN" :set-value set-value)
) ; }}}

(defun rule-icon-key (icon) ; file rule by its icon {{{
	(format nil "ICON  ~a" icon)
) ; }}}

(defun put-rule (rule  &key icon) ; {{{
	(let ((rule-tree *line-rule-table*) icon-str icon-len icon-key)
	;; get useful info from meta rule struct and then discard it
	(let ((line-data ; icon-str {{{
			;; LATER: accept either a LINE-RULE or a function symbol ?
			;;(if (line-rule-p rule)  rule  (funcall rule))
			(funcall rule)
		))
		(setq icon-str  ; override line rule's icon with :icon if provided
			(if (null icon)  (line-rule-icon line-data)  icon))
	) ; }}}
	
	;; get information about meta rule icon
	(setq
		icon-key (rule-icon-key icon-str)
		icon-len (length icon-str))
	
	;; file each substring in a meta rule as having a meta rule
	;; this aids identifying a meta line's function before breaking it down
	(loop for i from 0 to icon-len do
		(let ((substr (subseq icon-str 0 i)))
		;; try not to overwrite existing rules
		(unless (equal (gethash substr rule-tree) 'LINE-RULE-ICON)
			;; if substring is full-length, mark it as an icon, otherwise file T
			(if (equal i icon-len)
				(puthash substr 'LINE-RULE-ICON rule-tree)
				(puthash substr t rule-tree)))
	))
	
	;; update maximum length and put full rule {{{
	(let ((max-len (greatest-icon-length)))
		(when (> icon-len max-len)
			(greatest-icon-length :set-value icon-len))
	)
	
	;; put full rule
	(puthash icon-key rule rule-tree)
	;; }}}
	
)) ; }}}

(defun create-rule-table () ; create rule registry {{{
	;; create empty table
	(let ((rules *line-rules*) (rule-tree (make-hash-table :test 'equal)))
	(setq *line-rule-table* rule-tree)
	;; set table statistics
	(greatest-icon-length :set-value 1)
	
	(dolist (rule rules)
		(put-rule rule))
	
	;(debug-hash *line-rule-table*)
	
	t
)) ; }}}

(defun find-line-rule (icon) ; {{{
	(let ((key (rule-icon-key icon)) (rule-tree *line-rule-table*)
		)
	(gethash key rule-tree)
)) ; }}}

(defun find-meta-rules () ; {{{
;; used but not finished
	(create-rule-table)	
	nil
) ; }}}

;;; }}}


(defun line-meta-info (line) ; main meta line interpreter {{{
"see if a line contains meta information"
	(let ((len (length line)) (min-len 1) (rule-tree *line-rule-table*)
		kind-sym result)
	;; a line 0 chars long has no useful info, so discard it
	(when (> len 0)
	
	;; try to determine kind of meta line
	(let ((greatest-len (greatest-icon-length)) (greatest-i (- len 1)) greatest-icon-i)  ; {{{
		;; if greatest-len is longer than the whole line, make it the line's length
		(when (> greatest-len len)  (setq greatest-len len))
		(setq greatest-icon-i (- greatest-len 1))
		
		;; for each substring in meta line, try to look up in rule table
		(loop for i  from 0 to greatest-len  do   ; when (null kind-sym) 
			(let ((str (subseq line 0 i)) has-line)
			(setq has-line (gethash str rule-tree))
			
			(when
				(equal has-line 'LINE-RULE-ICON)  ; record representing a meta line
					(setq kind-sym (find-line-rule str))
					(setq min-len (length str))
			)
		))
		;; LATER: I tried to think of a way to stop the loop if the symbol was terminated with whitespace (so that symbols contained in other symbols could be recognised) but couldn't get it to work, so now it just checks all ~3-4 characters every time. at least that's not very many
	)  ; }}}
	
	;; if a meta line kind was found
	(unless (null kind-sym) ; {{{
		;; get info from line rule function
		(let ((rule (funcall kind-sym)) parser)
		
		;; previously this function looked for 'kind' as a string
		;; but now meta rules can have more information on them.
		;; LATER: remove :kind entirely
		(setq result
			(make-meta-line :kindsym kind-sym
				:kind (line-rule-name rule)
				:value (parse:rule-parser-value line min-len)
		))
		
		;; parse meta line according to its :parser
		;; skip function lines as they are supposed to be run as a table filter
		(when (null (line-rule-commandp rule))
			(setq parser (line-rule-parser rule))
			(unless (null parser)
				(setq result (funcall parser result))
				)
		)
	)) ; }}}
	
	result
))) ; }}}


;;; most basic line rules - rule-cxt , rule-rem {{{

(defun rule-cxt () ; {{{
	(make-line-rule :sphere "meta"  :icon "<=" :name "cxt" :label "context"
		:displayp t  :backlinkp t  :threadp t
		:parser 'parse:rule-parser-label-comment
)) ; }}}

(defun rule-rem () ; {{{
	(make-line-rule :sphere "meta"  :icon ";" :name "rem" :label "comment"
		:displayp nil
)) ; }}}

;; line rules live in their package as functions so that once they're registered they become anonymous data and bop doesn't have to remember what package they came from

;;; }}}


;;; add the most 'relevant' meta lines to tablerow {{{

(defun basic-links-filter (full-body) ; store link lines on tablerow {{{
	(let ((meta (bop-page-meta full-body)) outlinks backlinks misclinks)
	
	(dolist (line meta)
		(let ((rule
			(funcall (meta-line-kindsym line))
		))
		;; if meta line is a link to another entry, add to a link field
		(cond
			((line-rule-backlinkp rule)
				(push line backlinks))
			((line-rule-outlinkp rule)
				(push line outlinks))
			(t
				(push line misclinks)))
	))
	
	(setf
		(bop-tablerow-outlinks (bop-page-tablerow full-body)) outlinks
		(bop-tablerow-backlinks (bop-page-tablerow full-body)) backlinks
		(bop-tablerow-misclinks (bop-page-tablerow full-body)) misclinks
		)
	
	full-body  ; return modified page + tablerow
)) ; }}}

(defun basic-backlinks-filter (tablerow) ; copy backlinks over to the entry they should be on {{{
	(let (
		(links (bop-tablerow-backlinks tablerow))
		(replying-entry (bop-tablerow-idno tablerow))
	)
	
	(dolist (line links)  ; for every back link found in an entry (cxt/"<=", etc)
		(let ((row
			(page-row (meta-line-value line))
			))
		
		(when (table:page-exists-p nil :row row)  ; provided it points to a real row...
			;; add backlink to entry it "replies" to
			(setf (bop-tablerow-threadlinks row)
				(cons  ; with backlink now pointed to entry containing it
					;; make sure this is a new data structure to avoid destroying original link
					(duplicate-meta-line line :value replying-entry)
					(bop-tablerow-threadlinks row))
			)
			;; ensure changed row goes back into table
			(table:put-page row)
	)))
	
	tablerow
)) ; }}}

(defun alias-filter (tablerow) ; allow looking up entry through alias lines {{{
	(let ((meta
		(bop-tablerow-misclinks tablerow)))
	
	(dolist (line meta)
		(let ((rule (funcall (meta-line-kindsym line))) value)
		
		(when (line-rule-aliasp rule)
			(setq value (meta-line-value line))
			(unless (table:page-exists-p value)
				(table:put-page-alias value (bop-tablerow-basename tablerow)))
	)))
	
	tablerow  ; return modified page + tablerow
)) ; }}}

;;; }}}


;;; register filters & meta lines {{{

(register-table-filter 'basic-links-filter)
(register-tablepost-filter 'alias-filter)
(register-tablepost-filter 'basic-backlinks-filter)

(register-line-rules (list 'rule-cxt 'rule-rem))

;;; }}}
