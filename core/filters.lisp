;;; filters.lisp - filters to run when a new row is created
(defpackage :bopwiki-filters
	(:use :common-lisp
		:bopwiki-util :bopwiki-core-structs :bopwiki-uri :bopwiki-core-table
	)
	(:local-nicknames (:table :bopwiki-core-table))
	(:export
		#:*row-filters*
		#:new-row-filters
		
		; #:default-tagrules #:default-tags
		#:revision-of #:page-context-links #:page-outgoing-links #:page-tags #:page-date-line #:back-context-p
		#:apply-rule-if-tag #:default-tagrules-filter
		#:page-attributes-filter #:basic-context-filter #:date-line-filter #:page-tags-filter #:default-tags-filter
))
(in-package :bopwiki-filters)

;; LATER:
;; - clean up the mess of old hardcoded things
;; - every formatting rule filter should probably be moved to the formatting rules system


(defun default-filters () ; default set of row filters {{{
	(list
	'page-attributes-filter
	'basic-context-filter  'media-filter  'date-line-filter  'page-tags-filter
	'default-tags-filter  'default-tagrules-filter
)) ; }}}

(defparameter *row-filters* (default-filters))

(defun new-row-filters (page-body) ; run extra filters on row {{{
"Run extra processing steps on loaded entry files before putting them into the page table"
	(let ((tablerow (bop-page-tablerow page-body)) (filter-list *row-filters*))
	
	;; run through list of filters
	(dolist (filter filter-list)
		;; LATER: each filter currently *has* to take the two arguments tablerow and page-body.
		;;        update all filters to just take full-body
		(setq tablerow (funcall filter tablerow page-body))
		;; each filter usually alters tablerow,
		;; but set it just to be safe
	)
	
	;; put tablerow back into page-body and then return it
	(setf (bop-page-tablerow page-body) tablerow)
	page-body
)) ; }}}


;;; display portion {{{
;; LATER: this should be variables and there should be no 'display portions'

(defun default-tagrules() ; {{{
"Formatting rules which can be enabled by tag on an entry. If a rule is not in this list, it is currently disabled for the whole bopwiki folder"
	(list  "uni-hr" "md-h2" "h1" "md-li-x" "md-li" "uni-quote" "md-pre")
) ; }}}

(defun default-tags() ; {{{
"Which tags (as in \"#\" meta lines) will be applied by default to entries.
These do not have any effect by default, though with configuration they can be used to enable formatting rules"
	(list "h1")
) ; }}}

;;; }}}


;; filter helpers - revision-of, page-context-links, page-outgoing-links, page-tags, page-date-line
;; {{{

(defun revision-of (page) "return what page this is a revision of" ; {{{
	(let ((meta (bop-page-meta page)) result)
	
	;; for now, this function only connects each revision to one page
	(dolist (meta-line meta)
		(let ((kind (meta-line-kind meta-line))
			(value (meta-line-value meta-line))
		)
		(when (equal kind "ver")
			(setq result value))
	))
	
	result
)) ; }}}

(defun page-context-links (page) ; {{{
"PAGE - bop-page struct, aka 'full body'
Return list of context links in the meta section of PAGE, mainly for use in linksto field.
This does not modify any meta line data already in the page table."
	(let ((meta (bop-page-meta page)) (idno (bop-page-id page)) context)
	
	(dolist (line meta)
		(let (value)
		(when (and
			(equal (meta-line-kind line) "cxt")
			(not (has-protocol (setq value (meta-line-value line))))
			;; omit circular links to prevent infinite loops while rendering
			(not (equal value idno))
		)
		(setq context (cons value context))
	)))
	
	;; connect linkless pages to root when...
	(when (and
		(null context) (> idno 0)  ; - page has no context links & is not root
		(null (revision-of page))  ; - page is not a revision
		)
		(setq context (list "root"))
	)
	
	context
)) ; }}}

(defun page-outgoing-links (page) ; {{{
	(let ((meta (bop-page-meta page)) (idno (bop-page-id page)) context)
	
	(dolist (line meta)
		(let (value)
		(when (and
			(equal (meta-line-kind line) "fwd")
			(not (has-protocol (setq value (meta-line-value line))))
			;; omit circular links to prevent infinite loops while rendering
			(not (equal value idno))
		)
		(setq context (cons value context))
	)))
	
	context
)) ; }}}

(defun page-tags (page) ; {{{
	(let ((meta (bop-page-meta page)) result)
	
	(dolist (line meta)
		(let ((value (meta-line-value line)))
		(when (equal (meta-line-kind line) "tag")
			(setq result (cons value result))
			;; BUG: currently only one tag per line is supported
			;; comma / space separated values should be allowed
	)))
	result
)) ; }}}

(defun page-date-line (page idno) ; {{{
	(let ((meta (bop-page-meta page)) target)
	
	(loop for  line in meta  when (null target)  do
		(let ((value (meta-line-value line)))
		(when (and
			(equal (meta-line-kind line) "time")
			(is-valid-date value)
			)
		;; :: lines can accept iso or epoch dates
		(setq target (date-to-epoch value))
	)))
	
	(if (null target)  idno target)
)) ; }}}

(defun back-context-p (linking linked) ; tell if page was <= linked {{{
	;; all these functions are from core-table (?)
	(let ((linked-sources (bop-tablerow-linksfrom (table:page-row linked)))
		in-list)
	
	(loop for source  in linked-sources  while (null in-list) do
		(let ((source-id (table:page-id source)))
		(when (equal linking source-id) (setq in-list t))
	))
	in-list
)) ; }}}

;; }}}


;; tag filters - apply-rule-if-tag, default-tagrules-filter {{{

(defun apply-rule-if-tag (tag tablerow page-body  &key rule-row) ; add to bodyrules {{{
;; this is meant as a helper to write filters
	(let ((tags (bop-tablerow-tags tablerow))
		(bodyrules (bop-tablerow-bodyrules tablerow))
	)
	page-body rule-row  ; leave dummy alone
	
	(when (and (not-null tags) (string-member-p tag tags))
		(setf (bop-tablerow-bodyrules tablerow)
			(cons tag bodyrules))
	)
	
	tablerow
)) ; }}}

(defun default-tagrules-filter (tablerow page-body) ; {{{
	(let ((tag-rules (default-tagrules)))
	(dolist (rule tag-rules)
		(apply-rule-if-tag  rule  tablerow page-body))
	tablerow
)) ; }}}

;; }}}

;; basic filters - page-attributes-filter, basic-context-filter, media-filter, date-line-filter, page-tags-filter, default-tags-filter 
;; {{{

;; LATER: bop-pageattr may be replaced with something more flexible later
(defun page-attributes-filter (tablerow page-body) ; {{{
	;; add attributes to page: revision etc
	(setf (bop-tablerow-attr tablerow)
		(make-bop-pageattr
			;; determine if page is a revision
			:revision (revision-of page-body)
	))
	tablerow
) ; }}}

;; link page to other pages
(defun basic-context-filter (tablerow page-body) ; {{{
	;; <= links on a page meta section are 'links from'
	(setf (bop-tablerow-linksfrom tablerow)  (page-context-links page-body))
	;; => meta lines and connected replies are 'links to'
	(setf (bop-tablerow-linksto tablerow)  (page-outgoing-links page-body))
	
	tablerow
) ; }}}

;; media filter
(defun media-filter (tablerow page-body) ; {{{
	(let ((metas (bop-page-meta page-body)) (media-list nil))
	
	(dolist (meta metas)
		(let (value)
		(when (equal (meta-line-kind meta) "obj")
			(setq value (meta-line-value meta))
			(setq media-list (cons value media-list))
	)))
	;; LATER: unsure if these simple strings for kind will always be used
	
	(unless (null media-list)
		(setf (bop-tablerow-media tablerow) media-list)
	)
	tablerow
)) ; }}}

;; allow a :: line to change the page date
(defun date-line-filter (tablerow page-body) ; set date from :: line {{{
	(let ((date-line
		(page-date-line page-body (bop-tablerow-idno tablerow))
		))
	
	;; if a valid date was returned, replace the date in tablerow
	(unless (null date-line)
		(setf (bop-tablerow-date tablerow) date-line)
	)
	tablerow
)) ; }}}

;; tag entries for various purposes
(defun page-tags-filter (tablerow page-body) ; {{{
	(let ((tags (page-tags page-body)))
	
	;; if there are any tags set them
	(unless (null tags)
		(setf (bop-tablerow-tags tablerow) tags)
	)
	tablerow
)) ; }}}

(defun default-tags-filter(tablerow page-body) ; {{{
	(let ((default-tags (default-tags))
		(existing-tags (bop-tablerow-tags tablerow))
	)
	page-body  ; leave dummy alone
	(dolist (tag default-tags)
		(setq existing-tags (cons tag existing-tags))
	)
	(setf (bop-tablerow-tags tablerow)  existing-tags)
	tablerow
)) ; }}}

;; }}}
