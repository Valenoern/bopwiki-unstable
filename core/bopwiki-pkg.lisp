(uiop:define-package :bopwiki
	(:use :common-lisp)
	(:use-reexport
		:bopwiki-util
		:bopwiki-version
		:bopwiki-core-structs :bopwiki-uri :bopwiki-filesystem
		:bopwiki-linerules :bopwiki-oldrules-formatting :bopwiki-filters
		:bopwiki-linerules-render
		:bopwiki-core-table :bopwiki-core-entryfile :bopwiki-core-scanner
		:bopwiki-core :bopwiki-core-command
		:bopwiki-cmd-open :bopwiki-cmd-show
	)
	; mix , local-nicknames , use-reexport
)
