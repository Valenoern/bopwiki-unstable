;;; core-scanner - for finding entry files inside a bop stem
(defpackage :bopwiki-core-scanner  ; {{
	(:use :common-lisp
		:bopwiki-util :bopwiki-uri :bopwiki-filesystem
		;; bopwiki-core requires this package
	)
	(:export
		#:all-page-files
)) ; }}
(in-package :bopwiki-core-scanner)


(defun all-page-files (arg)  ; get all files in a bopwiki directory {{{
"Get all files in bopwiki directory ARG recursively, where ARG is a lisp pathname.
Currently this only opens entry files and not media attachments."
	(let (all-files files dirs dir (exclude-dirs (list ".bop" ".git")))
	
	(setq
		dir (surer-pathname arg) ; allow directory name to end in an extension like ".bop"
		files (uiop:directory-files dir)
		dirs (uiop:subdirectories dir))
	
	;; add all normal files to all-files list,
	;; trying to filter out files that aren't wiki pages
	(dolist (file files)
		;; LATER: allow finding media attachments and marking them appropriately, in order to look up their filenames in the page table - issues.bop/1610683500
		(if (page-file-p file)
			(setq all-files (cons file all-files))
	))
	
	(cond
		;; if no subdirectories, do nothing
		((null dirs) '())
		;; otherwise return the list of files inside
		(t (dolist (subdir dirs) ; {{{
			(let (dir-files stem)
			(setq stem (nth 0 (last (pathname-directory subdir))))
			
			;; attempt to exclude directories there should be no pages inside,
			;; such as .git and .bop
			;; LATER: allow arbitrarily adding things to exclude
			(unless (string-member-p stem exclude-dirs)
				;; for each subdirectory fetch files recursively with this function
				(setq dir-files (all-page-files subdir))
				;; add subdir's files ('leaves') to top file list
				(dolist (leaf dir-files)
					(setq all-files (cons leaf all-files))
			))
		))) ; }}}
	)
	
	all-files
)) ; }}}

;; media-directories is removed for now to redo when coding 'put attachments in table'
