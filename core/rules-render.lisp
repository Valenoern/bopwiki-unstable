;;; rules-render.lisp - "filters" useful for rendering and maybe commands {{{
(defpackage :bopwiki-linerules-render
	(:use :common-lisp :bopwiki-core-structs :bopwiki-util)
	(:local-nicknames
		(:table :bopwiki-core-table)
		(:rules :bopwiki-linerules)
	)
	(:export
		#:remove-duplicate-replies
))
(in-package :bopwiki-linerules-render)
;;; }}}


(defun remove-duplicate-replies (preview-links) ; deduplicate replies on same parent {{{
	(let (row-names already-printed results)
	
	;; pick out replies from preview-links
	(setq row-names ; {{{
		(mapcan
			#'(lambda (x)
				(if (rules:line-rule-threadp (funcall (meta-line-kindsym x)))
					(list  (table:page-basename (meta-line-value x)))
					nil
			))
			preview-links)
	) ; }}}
	
	(dolist (link preview-links)
		(let (
			(rule (funcall (meta-line-kindsym link)))
			(link-idno
				(table:page-basename (meta-line-value link)))
		)
		
		(unless  ; keep preview unless...
			(or
				(null link-idno)  ; it has no row
				;; it is a see-also duplicated by a reply
				(and (null (rules:line-rule-threadp rule)) (find link-idno row-names))
				;; it has already been printed on this parent
				(find link-idno already-printed)
				)
			(push link-idno already-printed)  ; don't add this preview subsequently
			(push link results)  ; add preview to final results
			)
	))
	
	(reverse results)  ; return results in forward order
)) ; }}}
