#!/bin/sh

# USER SETTING:
# tell which worktrees to update separated by spaces -
# each worktree name should be a folder name inside /path/to/[.git or bare folder]/worktrees
update_trees="distri"

# NO user settings below this line


tree_info=""
tree_dir_git=""
tree_dir=""

for tree in ${update_trees}
do
  tree_info="worktrees/${tree}"

  # if worktree is listed in worktrees, load its path
  if [ -d $tree_info ]; then
    tree_dir_git=`cat "worktrees/${tree}/gitdir"`
    tree_dir=`dirname $tree_dir_git`

    # if worktree's path exists, update it
    if [ -d $tree_dir ]; then
      cd $tree_dir
      echo "Resetting worktree $tree - $tree_dir"
      git -C $tree_dir reset --hard

      # BUG: trees properly update but it says . is not a valid repo?
    fi

    # note: it's possible to have dangling worktrees with incorrect paths
    # if so they will not be updated.
  fi
done


# because i use this for updating bop exports,
# temporarily storing it in bopwiki repo while figuring out a better place
