#!/bin/sh

key_name=$1

if [ -z "$1" ]; then
  echo "Usage: keygen.sh <key_label>"

  else
  # referenced from:
  # https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/
  openssl genrsa -out ${key_name}.pem 2048 && \
  openssl rsa -in ${key_name}.pem -outform PEM -pubout -out ${key_name}-pub.pem
fi
