;;; feed-author.lisp - feed metadata {{{
(defpackage :bop-render-atom-author
	(:use :common-lisp :bopwiki :bop-render-core
	)
	(:local-nicknames
		(:table :bopwiki) (:render :bop-render-core)
		(:author :bop-render-author)
		;; uiop
	)
	(:export
		#:feed-meta-title #:feed-meta-tag #:feed-meta-link #:feed-meta-updated #:feed-meta-updated-str #:feed-meta-namespace
		#:feed-meta-authorname #:feed-meta-authorpath #:feed-meta-username #:feed-meta-domain
		#:feed-meta-pagesdir #:feed-meta-generatorver #:make-feed-meta
		
		#:atom-meta-template
		
		#:atom-tag-uri
		
		#:atom-feed-meta
))
(in-package :bop-render-atom-author)

(defstruct feed-meta ; {{{
	title tag link updated updated-str namespace
	authorname authorpath username domain
	pagesdir
	generatorver
) ; }}}

;;; }}}


(defun atom-meta-template (meta) ; {{{
"Render atom feed meta information based on supplied META struct."

	(format nil
"<id>~a</id>
<updated>~a</updated>
<title>~a</title>
<author>
	<name>~a</name>
	<uri>~a</uri>
</author>
<link rel=\"self\" href=\"~a\" />
<generator uri=\"~a\" version=\"~a\">bopwiki</generator>"
		;; id + updated are at the top so smart feedreaders could stop downloading an old feed
		(feed-meta-tag meta)  (feed-meta-updated-str meta) ; tag uri, updated
		(feed-meta-title meta)  ; title
		(feed-meta-authorname meta)  (feed-meta-authorpath meta)  ; author
		(feed-meta-link meta)  ; self link
		(bop-url) (feed-meta-generatorver meta)  ; generator
)) ; }}}


;; OUTDATED
(defun atom-feed-title () ; {{{
"Should return a string representing the title of a generated atom feed.
By default, returns nil and leaves title to be auto-generated"
	nil
) ; }}}

(defun fallback-atom-title (bop-user bop-domain) ; {{{
	(format nil
	"bopwiki Atom feed — ~a@~a" bop-user bop-domain
)) ; }}}

(defun atom-tag-uri (bop-user bop-domain feed-date item) ; {{{
	(format nil "tag:~a@~a,~a:~a"  bop-user bop-domain feed-date item)
) ; }}}


(defun atom-feed-meta (&key bop-version) ; assemble meta struct {{{
	(let (
		bop-user bop-domain bop-author feed-title
		feed-created feed-created-str update-date feed-taguri author-path pages-dir feed-path
		;; get feed author / "AP actor" info from rdf file
		(actor-info (author:test))
	)
	
	;; user info - ex. valenoern , distributary.network
	(setq bop-user (gethash "http://distributary.network/owl-wing/2021-06/bop#username" actor-info))
	(setq bop-domain (gethash "http://distributary.network/owl-wing/2021-06/bop#host" actor-info))
	
	;; author display name - ex. valenoern
	(setq bop-author bop-user)
	;; feed title - ex.: bopwiki Atom feed — valenoern@distributary.network
	;; LATER: I don't like the setup of these two functions, they feel like they should be one
	(when (null (setq feed-title (atom-feed-title)))
		(setq feed-title (fallback-atom-title bop-user bop-domain))
	)
	
	;; created, updated
	(setq feed-created (gethash "http://distributary.network/owl-wing/2021-06/bop#created" actor-info))
	
	(setq feed-created-str
		(if (null feed-created)
			""  (epoch-to-iso feed-created :date-only t))
	)
	
	;; LATER: actually get date of latest entry
	(setq update-date feed-created)
	(setq updated-str
		(if (null update-date)
			""  (epoch-to-iso update-date :date-only t))
	)
	
	;; id - ex. tag:valenoern@distributary.network,2003-12-13T18:30:02Z:feed/atom
	(setq feed-taguri (atom-tag-uri bop-user bop-domain feed-created-str "feed/atom"))
	;; url for exported entries
	;; LATER: you may want this to be different for each format
	(setq pages-dir (format nil "http://~a/bop/~a/"  bop-domain bop-user))
	;; self link - ex. distributary.network/bop/valenoern/feed
	(setq feed-path (format nil "http://~a/bop/~a/atom.xml"  bop-domain bop-user))
	;; author path - ex. distributary.network/bop/valenoern/
	(setq author-path pages-dir)
	
	;; mostly for testing purposes, a specific bop version string can be passed
	;; otherwise, try to get the current bop version
	(when (null bop-version)  (setq bop-version (bop-version)))
	
	(make-feed-meta ; {{{
		:title feed-title
		:authorname bop-author  :authorpath author-path
		:username bop-user  :domain bop-domain
		:tag feed-taguri  :link feed-path
		:updated update-date
		:updated-str (epoch-to-iso update-date)
		;; entire first part of tag uri before item name
		:namespace
			(atom-tag-uri bop-user bop-domain feed-created-str "")
		:pagesdir pages-dir
		:generatorver bop-version
	) ; }}}
)) ; }}}
