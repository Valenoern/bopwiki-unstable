;;; uri.lisp - web uri abstraction {{{
(defpackage :bop-render-uri
	(:use :common-lisp :bopwiki)  ;:bop-render-core
	(:local-nicknames
		(:bop :bopwiki)
		;; (:puri :puri)
	)
	(:export
		;; WEB-LINK
		#:web-link #:web-link-rel #:web-link-href #:web-link-path #:web-link-mimetype #:web-link-p
		#:construct-web-link  ; make-web-link omitted in favour of this
		#:render-web-link
		
		;; WEB-AUTHOR
		#:web-author #:web-author-handle #:web-author-host #:web-author-path #:web-author-p #:make-web-author
))

;; LATER:
;; an EXPERIMENT in moving render-AP's uri functions to render.
;; they will be removed from render-AP if/when it's safe to.
;; this package may also move along with render-author if/when it moves out of bop-render.

(in-package :bop-render-uri)
;;; }}}


;;; WEB-LINK {{{

(defstruct web-link  ; rel href path mimetype {{{
;; lisp struct originally made to represent ActivityStreams Link object
	rel  ; html-style relation property
	href  ; puri format uri [contains :path]
	path  ; path on server as a lisp pathname
	mimetype  ; content type string, "text/html" etc
) ; }}}

(defun construct-web-link ; create uri struct {{{
	(&key scheme host path  name type  rel mimetype)
	(let (lisp-filename
		(host
			;; if host is expressed as a PURI:URI, flatten it
			(if (puri:uri-p host)  (puri:render-uri host nil)  host))
		(filetype type) ; though 'type is allowed it's highlighted in my editor
		(full-path
			;; if path is expressed as a namestring, parse it
			(cond
				((stringp path)  (parse-namestring path))
				((web-link-p path) (web-link-path path))
					;; web-link is not /supposed/ to go in here but it somehow happened
				(t path))
		)
	)
	
	;; if defined, merge :name and :type into uri path
	(unless (null name)
		(setq lisp-filename (make-pathname :name name :type filetype))
		(setq full-path (merge-pathnames full-path lisp-filename))
	)
	
	(make-web-link
		:rel rel
		:path full-path  ; lisp pathname
		:mimetype mimetype
		:href
			(make-instance 'puri:uri
				:scheme scheme  ; must be a symbol, e.g. 'https
				:host host
				:path (namestring full-path))
	)
)) ; }}}

(defun render-web-link (uri) ; render WEB-LINK uri to string {{{
	(cond
		;; if URI is a WEB-LINK, render its "href" slot
		((web-link-p uri)
			(puri:render-uri (web-link-href uri) nil))
		;; if URI is a string etc, return it unchanged
		(t uri))
		
		;; LATER: also accept regular puri URIs
) ; }}}

;;; }}}

;;; WEB-AUTHOR {{{

(defstruct web-author
;; originally I made an 'actor' struct solely for activitypub, but then I discovered I need one to even render absolute web urls.
;; LATER: figure out what general applicable-to-all-formats features this really needs
	handle  ; username, e.g. "user"
	host  ; host name as string, e.g. "example.com" in "user@example.com"
	path  ; format string, e.g. "/bop/~a/"
)

;;; }}}
