;;; bop-render-publishgroup
(defpackage :bop-render-publishgroup ; {{{
	(:use :common-lisp :bopwiki :bop-render-core :bop-render-targets)
	(:local-nicknames (:table :bopwiki) (:targets :bop-render-targets))
	(:export
		#:publish-group-data #:publish-group-subset
		#:publish-bop-pubgroup #:register-publish-group
)) ; }}}
(in-package :bop-render-publishgroup)


(defun publish-group-data (group) ; {{{
;; flatten publish group into simple data
	(let ((targets (publish-target-pages group)) target (result nil))
	;; for each target name, flatten target to its 'canonical' form
	(dolist (target-name targets)
		(setq target (parse-wants target-name))
		(setq result (cons target result))
	)
	result
)) ; }}}

(defun publish-group-subset (group) ; {{{
;; LATER: I think I intended :data vs. :pages to be used to aid this. but fix that later
"Get the entire subset of pages required to publish a publish target, for use by subset-tables"
	(let ((data (publish-group-data group)) (found-pages (make-hash-table :test 'equal)) subset)
	
	(dolist (target data)
		(let ((pages (publish-target-pages target)))
		(dolist (pagename pages)
			(unless (equal (gethash pagename found-pages) t)
				(setq subset (cons pagename subset)))
			(puthash pagename t found-pages)
		)
	))
	
	subset
)) ; }}}

(defun publish-bop-pubgroup (group pwd) ; {{{
;; publish format to publish multiple publish groups
	;; flatten publish targets to canonical lists of pages
	(let ((data (publish-group-data group)) func)
	(dolist (target data)
		;; get function which carries out publish target, and run it
		(setq func (publish-target-func target))
		(funcall func target pwd)
	)
)) ; }}}

(defun register-publish-group (&key name targets) ; {{{
"Create publish target group and file in targets table using standard publish targets feature.
NAME is the string used to look up a publish group, via 'bop publish NAME'.
TARGETS should be a list of normal target names as strings."

	(targets:register-publish-target :name name  :data targets  :func 'publish-bop-pubgroup
		:expander 'publish-group-subset)
) ; }}}
