#!/bin/sh

# change to script directory
cd "${0%/*}"; cd ..
# set background variables
bopdir=`pwd`
repodir=${bopdir}/build-src
reposubdir=""


# installer for debian-based distros including devuan & ubuntu
install_debian() {
 # dereference links
 bop_runlisp=`readlink ${repodir}/bop-runlisp.deb`
 bop_links=`readlink ${repodir}/bop-dev-links.deb`

 # try to install packages
 echo "Installing .deb packages"
 sudo apt install "$bop_runlisp" "$bop_links"

 link_source
}

# installer for arch-based distros
install_arch() {
 # makepkg doesn't like installing from links, so dereference them
 bop_runlisp=`readlink ${repodir}/bop-runlisp.zst`
 bop_links=`readlink ${repodir}/bop-dev-links.zst`

 # try to install packages
 echo "Installing .tar.zst packages"
 sudo pacman -U "$bop_runlisp" "$bop_links"

 link_source
}

link_source() {
 # link bop source directory to /usr/share/bopwiki,
 #   removing it if it already exists
 echo "Linking source directory ${bopdir} to /usr/share/bopwiki"
 if [ -e /usr/share/bopwiki ]; then
  sudo rm /usr/share/bopwiki
 fi
 sudo ln -s "${bopdir}" /usr/share/bopwiki

 echo "If you move or accidentally delete this git repository, rerun this installer."

 # check for a working ~/.sbclrc, create if not present
 ./installer/sbclrc.sh
}
# BUG: /usr/share/bopwiki is linking again inside itself

main() {
 # detect package manager
 distro=`./installer/distro.sh`

 if [ "$distro" = "arch" ]; then
  install_arch
 elif [ "$distro" = "debian" ]; then
  install_debian
 else
  echo "No packages to install"
 fi
}

main
