#!/bin/sh

# change to script directory
cd "${0%/*}"; cd ..
# set background variables
bopdir=`pwd`
repodir=${bopdir}/build-src
reposubdir=""
# paths to clone packages
pkg_ytp=https://codeberg.org/Valenoern/bop-ytp-pkgbuild.git
#pkg_bop=/path/to/bop-pkg


# build debian package (.deb)
make_debian() {
 # note: only tested on refracta/devuan, not on ubuntu

 # clone pkgbuild repositories if they don't already exist
 mkdir -p ${repodir} && cd ${repodir}
 if [ ! -d "bop-ytp" ]; then  git clone ${pkg_ytp} "bop-ytp"; fi

 # build packages in bop-ytp monorepo
 reposubdir="${repodir}/bop-ytp"
 cd "${reposubdir}"
 # unless package exists, build run-lisp command
 cd "${reposubdir}/runlisp"
 if [ ! -e bop-runlisp*.deb ]; then make deb; fi
 # build links package to use development version of bop
 cd "${reposubdir}/bop-links"
 if [ ! -e bop-dev-links*.deb ]; then make deb; fi

# link packages for convenience
 if [ ! -e ${repodir}/bop-runlisp.deb ]; then
  ln -s ${repodir}/bop-ytp/runlisp/bop-runlisp*.deb ${repodir}/bop-runlisp.deb
 fi
 if [ ! -e ${repodir}/bop-dev-links.zst ]; then
  ln -s ${repodir}/bop-ytp/bop-links/bop-dev-links*.deb ${repodir}/bop-dev-links.deb
 fi

 echo "Built packages in /build-src"
}

# build arch package (.tar.zst)
make_arch() {
 # note: only tested on manjaro.

 # clone pkgbuild repositories if they don't already exist
 mkdir -p ${repodir} && cd ${repodir}
 if [ ! -d "bop-ytp" ]; then  git clone ${pkg_ytp} "bop-ytp"; fi

 # build packages in bop-ytp monorepo
 reposubdir="${repodir}/bop-ytp"
 cd "${reposubdir}"
 # unless package exists, build run-lisp command
 cd "${reposubdir}/runlisp"
 if [ ! -e bop-runlisp*.zst ]; then make; fi
 # build links package to use development version of bop
 cd "${reposubdir}/bop-links"
 if [ ! -e bop-dev-links*.zst ]; then make; fi

 # link packages for convenience
 if [ ! -e ${repodir}/bop-runlisp.zst ]; then
  ln -s ${repodir}/bop-ytp/runlisp/bop-runlisp*.zst ${repodir}/bop-runlisp.zst
 fi
 if [ ! -e ${repodir}/bop-dev-links.zst ]; then
  ln -s ${repodir}/bop-ytp/bop-links/bop-dev-links*.zst ${repodir}/bop-dev-links.zst
 fi

 echo "Built packages in /build-src"
}

main() {
 # detect package manager
 distro=`./installer/distro.sh`

 if [ "$distro" = "arch" ]; then
  echo "Arch-style package manager found - building .tar.zst package"
  make_arch
 elif [ "$distro" = "debian" ]; then
  echo "Debian-style package manager found - building .deb package"
  make_debian
 else
  # echo to stderror
  echo "No supported package manager found. Try submitting an issue to get your package manager supported" >&2
 fi
}

main
