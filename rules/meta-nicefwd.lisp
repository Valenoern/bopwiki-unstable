;;; meta-basic.lisp - semi-core meta line rules {{{
(defpackage :bop-rules-meta-nicefwd
	(:use :common-lisp :bopwiki)
	(:local-nicknames 
		(:pcre :cl-ppcre)
		(:parse :bopwiki-linerules-parse)
		;;(:rules :bopwiki-linerules)
		(:basic :bop-rules-meta)
	)
	(:export
		#:rule-parser-citefwd
		#:rule-fwd-cite
))
(in-package :bop-rules-meta-nicefwd)

;; note: this package is named "nicefwd" instead of "citefwd" in case further alternate see-alsos are added later.
;;; }}}


;; LATER: display footnote codes in render-html

(defun rule-parser-citefwd (line) ; {{{
	(let ((value (meta-line-value line)) dummy)
	dummy
	
	;; example meta line:
	;; #> [1.] Link Label | https://example.com invisible-comment
	
	;; extract information using regular expression
	(pcre:do-register-groups (cite-code link-label link-value)
		((format nil
			;; \s* - zero or more spaces;  ~A - region of regex
			"^\\[~A\\.?\\]\\s*~A\\s*\\|\\s*~A\\s*~A$"
				"([^\\[\\]\\.]+)" ; $1 - footnote code
				"(.+)"            ; $2 - link label
				"([^\\s]+)"       ; $3 - link url
				".*"              ; discarded - invisible comment
			)
			value  nil  :sharedp t)
		
		(setf
			(meta-line-label line) (string-right-trim " " link-label)
			(meta-line-value line) link-value
			(meta-line-num line) cite-code
				;; returned as string because it could easily be a letter
		))
	
	line  ; return meta line containing parsed information
)) ; }}}


(defun rule-fwd-cite () ; alternate =>/fwd starting with "[n.] Label" {{{
"Alternate see-also meta line (LINE-RULE struct) optimised for readable web URLs and adding footnote codes.
Must be manually activated when configuring your bop stem; see bopwiki manual"

	;; use the "=>"/fwd line and change only a few things
	(let ((rule (basic:rule-fwd)))
	(setf
		;; (line-rule-icon rule) "=>"
			;; "#>" can be used to distinguish from regular "=>"
			;; but I thought it didn't look as good on a published page
		(line-rule-parser rule) 'rule-parser-citefwd
	)
	rule
)) ; }}}
