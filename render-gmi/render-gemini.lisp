;;; render-gmi.lisp - gemini renderer {{{
(defpackage :bop-render-gemini
	(:use :common-lisp :bopwiki :bop-render-core :bop-render-targets
		:bop-render-gemini-filenames
	)
	(:local-nicknames
		(:table :bopwiki) (:render :bop-render-core)
		(:page :bop-render-gemini-page)
		;; uiop
	)
	(:export
		;; config functions
		#:publish-gemini-path #:*publish-gemini-path*
		
		#:publish-bop-gemini
))
(in-package :bop-render-gemini)
;;; }}}


;;; config functions - publish-gemini-path {{{

(defparameter *publish-gemini-path* nil) ; {{{

(defun publish-gemini-path (path)
"Set directory to publish rendered entries to when running 'bop publish', using the lisp pathname PATH.
This function is designed to be used in init.lisp, and to nest any arbitrary function to calculate the pathname, ex. (namestring \"/path/to/output\")"
	(let ((path-object (uiop:ensure-pathname path)))
	;; BUG: check?
	(setq *publish-gemini-path* path-object)
))
;; }}}

;;; }}}


(defun publish-bop-gemini (target cwd) "" ; {{{
	(let (output page-list)  ;(cwd (surer-pathname cwd))
	cwd  ; unused in this format
	
	;; if no publish path, use the bop-render one
	(when (null *publish-gemini-path*)
		(publish-gemini-path render:*default-publish-path*)
	)
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname *publish-gemini-path*))
	(ensure-directories-exist output)
	
	;; publish formats now take a standardised 'publish target' struct
	(setq page-list (publish-target-pages target))
	
	;; render individual non-revision pages
	(dolist (pagename page-list)
		(let (full-body page-id rendered-file raw-file)
		(setq
			full-body (table:page-body pagename)
			page-id (table:page-id pagename)
			rendered-file (exported-page-url pagename "gmi")
			raw-file (page-source-filename page-id)
		)
		(export-in-dir output rendered-file
			(page:render-entrypage-gemini pagename))
		(export-in-dir output raw-file (page:raw-page-gemini full-body))
	))
	;; with gemini there is no stylesheet file
	
	nil
)) ; }}}
