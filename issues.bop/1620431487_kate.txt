BUG: editor called is always "kate", user must symlink other editor to "kate"

* X allow choosing editor in init.lisp [ver 2022-6-07]
* X open arbitrary graphical editor

* research how to correctly open command-line editors
*  * preset package for any helper functions needed for known editors such as nano, vim, emacs?
*  * chunk out behaviours to specify new editors in init.lisp


# md-li-x
# md-li
<= 1608963724 open
