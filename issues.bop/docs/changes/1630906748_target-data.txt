21-09-05: init.lisp query functions

when publish targets were first created I hardcoded the ability to get directories with "dirs":
```
(register-publish-target :name "example" :func 'bop-render:publish-bop-html
	:dirs (list "dir1" "dir2"))
```

now everything has been reworked so publish targets are created with a flat page list, which can be populated using functions (showing the true power of lisp S-expressions if you ask me):
```
(register-publish-target :name "example" :func 'bop-render:publish-bop-html
	:data (list (dir-pages "dir1") (dir-pages "dir2") (tag-pages "tag_3")))
```

there is also a very experimental "valenoern shortcuts" system to create publish targets in a shorter syntax.
```
(publish-html "example" (list (dir-pages "dir1") (dir-pages "dir2")))

(publish-target "ap" 'ap:publish-bop-activitypub (list (dir-pages "dir1") (dir-pages "dir2")))
```

<= 1620355787 breaking changes
# md-pre
