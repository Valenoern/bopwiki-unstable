lisp function docstrings are used for "API documentation" - somewhat dry/technical explanations of exactly in what coding context a programmer should use this code and why.

comments are largely used as reminders of things - what still needs to be done, if something doesn't work exactly the way it's intended to, if you dived into a source file blind and were supposed to know additional information first where to find that

issue tracker entries are like ascended comment lines. often a comment is essentially supposed to become an issue entry but I forget to do that, or feel like the problem will be fixed too fast to bother, or maybe don't have a good enough idea how to fix the problem to feel like it's worth one yet.

the user manual, like the other three, holds its own specific kind of information.
guides for complete newbies who have never looked at the code (and perhaps barely will at all),
guides related to installing, contributing, etc.,
'historical' or 'tangential' things which are useful but aren't necessary to clutter the source files with (e.g. breaking changes and why they were made, weird choices bop-render AP made to interoperate with mastodon).


<= 1630738412 conventions
