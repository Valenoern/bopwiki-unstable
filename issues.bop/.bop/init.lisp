;; configure and customise bopwiki's rendering behaviour here.
;; see docs.bop for detailed examples on what you can do
;;   (once the docs are less under-construction.)

(bop:load-extensions "bop-render-html")

(defpackage :bopwiki-init
	(:use :common-lisp :bop-render-core)
	(:local-nicknames
		(:html :bop-render-html-2)
))
(in-package :bopwiki-init)


(default-publish-path
	(parse-namestring "/tmp/issues-bop/"))
	;; on my local machine I have this linked to the folder where issues.bop publishes,
	;; and use it to push the manual to distributary.network
	;;(parse-namestring "/mnt/issues-bop-publish/issues-bop/"))

(post-cutoff-limit 600)
(post-excerpt-limit 800)
