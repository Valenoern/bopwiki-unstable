;;; render-html.lisp - stuff specific to rendering an html 'website' {{{
(defpackage :bop-render-html-2
	(:use :common-lisp :bopwiki :bop-render-core :bop-render-targets
		:bop-render-html-page
	)
	(:local-nicknames
		(:table :bopwiki) (:render :bop-render-core)
		(:template :bop-render-html-template)
		(:style :bop-render-html-style)
		;; uiop
		;; osicat
	)
	(:export
		;; config functions
		#:publish-html-path #:*publish-html-path*
		
		;; export-html-synonymised
		
		;; #:render-entrypage-html - removed
		#:publish-bop-html
))
(in-package :bop-render-html-2)


;; config functions - publish-html-path {{{

(defparameter *publish-html-path* nil)

(defun publish-html-path (path)
"Set directory to publish rendered entries to when running 'bop publish', using the lisp pathname PATH.
This function is designed to be used in init.lisp, and to nest any arbitrary function to calculate the pathname, ex. (namestring \"/path/to/output\")"
	(let ((path-object (uiop:ensure-pathname path)))
	;; BUG: check?
	(setq *publish-html-path* path-object)
))

;; }}}

;;; }}}


(defun export-html-synonymised (output-dir pagename page-content) ; {{{
"export entry to canonical + short url"
	(let (
		(html-file (exported-page-url pagename "htm"))
		(symlink-name
			(merge-pathnames
				output-dir
				(make-pathname
					:name (format nil "~a" (page-id pagename)) :type "htm")
		))
		(canonical-path
			(make-pathname
				:directory '(:relative ".") :name pagename :type "htm"))
	)
	
	;; output regular file
	(export-in-dir output-dir html-file page-content)
	
	(unless (uiop:file-exists-p symlink-name)
		(osicat:make-link symlink-name :target canonical-path))
		;; - this assumes symlinks are possible
		;; - redirecting .html to .htm should be handled in nginx config. LATER: note in manual
	
	nil
)) ; }}}


;;; "bop publish" command {{{

(defun publish-bop-html (target cwd) "publish wiki folder to html" ; {{{
	(let (output page-list)
	cwd  ; required but unused argument
	
	;; basic publish stuff which will be / has been revamped on another branch {{{
	
	;; if no publish path, use the bop-render one
	(when (null *publish-html-path*)
		(publish-html-path render:*default-publish-path*))
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname *publish-html-path*))
	(ensure-directories-exist output)
	
	;; publish formats now take a standardised 'publish target' struct,
	;; to better enable updates to how 'bop publish' works without updating individual formats
	(setq page-list (publish-target-pages target))
	
	;; }}}
	
	;; run tweaks to entries if necessary
	;; LATER: create some kind of loop pages macro so people designing formats don't have to remember this
	(render:render-filters target)
	
	;; render individual non-revision pages
	(dolist (pagename page-list)
		(let ((full-body (table:page-body pagename :with-raw t)) rendered)
		
		;; this is the "default" arrangement of things on an html thread page
		;; LATER: possibly allow choosing another function which is called here
		(setq rendered
			(template:html-thread-page
				pagename
				(render-entry-html pagename :replyp nil :body full-body)
				(template:html-replies-section (render-replies-html full-body :nesting 1))
		))
		(export-html-synonymised  output pagename rendered)
	))
	
	;; export stylesheet
	(export-in-dir  output (make-pathname :name "style" :type "css")  (style:html-stylesheet))
)) ; }}}

;; set this format as the default 'bop publish'
;; this can be overridden in init.lisp
(render:default-publish-format 'publish-bop-html)

;;; }}}
