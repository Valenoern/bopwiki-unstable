;;; 'entry point' file, to run bop from a shell like bash

;; in case you started the program from a terminal, make sure all asdf systems are loaded
(when (null (find-package :bopwiki))  (asdf:load-system "bopwiki-cli"))

(defpackage :bopwiki-terminal-interface
	(:use :cl :bopwiki)
	(:local-nicknames
		(:bop :bopwiki) (:bop-fs :bopwiki-filesystem)
	)
	(:export
		#:main
))
(in-package :bopwiki-terminal-interface)


(defun main () ; {{{
	;; get args passed to bop
	(let ((args (uiop:command-line-arguments)))
	(let ((pwd (uiop:getcwd)) (cmd (nth 0 args)) (wants (nth 1 args)) (action (nth 2 args)) ; {{{
		;; the ./bop command passes args to this file as e.g. '(open pagename)
		;; 
		;; cmd ($1)  - command given to bop
		;; wants ($2) - what page, etc command acts on
		;; action ($3) - what to do with page
		)
	;; }}}
	
	(bop:init)  ; initialise data directory, init.lisp, etc.
	
	;; try to look up command in registered commands
	(bop:do-command cmd  :pwd pwd  :wants wants  :action action)
	
	;; individual bop commands are registered by loading the system that contains them.
	;; see below for where to find the default commands,
	;; and  core/cmd.lisp  for more info on how to write commands.
))) ; }}}


;;; core commands and their locations:  {{{
;; LATER: this is a little bit outdated

;; bop new [X]     core/cmd-open.lisp  open + edit new page
;; bop open X      "                   open specific page
;; bop rename X Y  "                   change the nickname/filename of page X to Y

;; bop list        "/cmd-show.lisp     list all pages visible from here

;; bop today [X]   "/cmd-journal.lisp  edit journal page for today, or set it given page name X
;;                                       [temporarily disabled due to bug]


;;; commands from 'stock' extensions:

;; bop publish [X] render/*            publish microwiki folder (or subdirectory X) to webpages
;;                                       by default, renders in html
;; bop test        test/test.lisp      run tests for all 'stock' parts of bop
;;                                       (only present if you've installed fiveam)

;;; }}}
